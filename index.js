var varsession = require("varsession");

function getDevice( req ){
	 var device,
	 	 varSession = varsession(req), 
	 	 stored = varSession("deviserve");

	 if (stored) {
		return stored;
	 }
	 else {
		var MobileDetect = require('mobile-detect');
		var md = new MobileDetect(req.headers['user-agent']);
		device = md.phone() ? "P" : (md.tablet() ? "T":"D")
		varSession("deviserve",device);
		return device;
	 }
	}

function getMiddleware(){
	var handlers = {},root={D:"", T:"", P:""};

	function on(ext, handler){
		handlers[ ext ] = handler;
		return middleware;
	}
	function path(dir){
		if (typeof dir == "string") root = {'D':dir+'/desktop/', 'T':dir+'/tablet/', P:dir+'/phone/'};
		else {
			root.D = dir.desktop || root.D;
			root.T = dir.tablet || root.T;
			root.P = dir.phone || root.P;
		}
		return middleware;
	}
	function rootpath(req){
		return root[getDevice(req)];
	}
	function sendFile( req, res, filename ){
		var onError = new Function();
		res.sendFile(filename, {}, function(err){if (err) onError(err);});
		return {error:function(e){onError=e;}};
	}

	function middleware(req, res, next){
		 var filename = require("path").join(root[getDevice(req)], req.path );
   	  	 var ext = filename.match(/\.\w+$/);
   	  	 if (ext && handlers[ ext[0] ]) {
   	  	 	handlers[ ext[0] ](req, res, filename);
   	  	 }
   	  	 else {
   	  	 	sendFile(req , res , filename ).error( function(){res.status(404).end();});
   	  	 }
   }

   middleware.on = on;
   middleware.path = path;
   middleware.root = rootpath;
   return middleware;
}

module.exports = getMiddleware;