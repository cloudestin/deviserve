# deviserve #

**deviserve** is a *node.js* package that analyses the user agent of the browser and gives information about device type (desktop, tablet or phone).

## Using sessions ##
If you are using express-session middleware, the information about device is stored as a session variable to avoid re-analyzing the user agent and lose precious machine cycles, which make it suitable for big multi-device applications.

```javascript
var expressSession = require('express-session');

app.use( expressSession({secret:'mySecretKey', cookie: { maxAge: 60000 }, resave:false, saveUninitialized:true}) );
```
The **deviserve** offers a middleware that serves static files from different directories depending on the device.

## Middleware ##
The **deviserve** middleware serves static files from different directories depending on the device.

```javascript
var deviserve = require("deviserve");
var Deviserve = deviserve()
    .path( __dirname+"/public/" )
    .on(".htm",renderJade)
    .on(".jade":page404);

function renderJade(req, res, filename){
    filename = filename.slice(0,-4)+".jade";
    require("jade").renderFile(filename , function(err,html){if (err) {res.send(500);} else {res.send(200,html);}});
}

function page404(req, res, filename){
    res.send(404).end();
}

app.use( "/pub", Deviserve );
```

The code above permits to serve static files fom different directories when *url* starting with `/pub/...` : files are served from `/public/desktop/`, `/public/tablet/` and `/public/phone/`.

In addition, the *url* with `.htm` extension is served from a `.jade` compiled file and `.jade` files are excluded from visualisation.

## Specific paths ##
The ```.path( )``` function can receive an object as its first argument. In this case, the specific root directory is specified for each device, as in the example bellow :

```javascript
var deviserve  = require('deviserve');
var Deviserve = deviserve().path({
    desktop:__dirname+"/views/desktop/",
    tablet:__dirname+"/views/mobile/",
    phone:__dirname+"/views/mobile/"
  });

app.use( "/pub", Deviserve );
```

In this case, tablets and phones files are served from /moblile/ subdirectory where desktop devices files are served from /desktop/ directory/

## Other functions ##
This package offers one other function named ```root()```. ```Deviserve.root(req)``` returns the root directory on the current device :

```javascript
Deviserve.root(req)
```


Back to [backend](https://bitbucket.org/rouabhi/bizsuite/wiki/Backend)